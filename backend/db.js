const mysql = require('mysql2')

const pool = mysql.createPool({
host: 'examprep',
user: 'root',
password: 'root',
database: 'examprep',
waitForConnection: true,
connectionLimit: 10,
queueLimit: 0,
port: '3306'


})

module.exports =pool